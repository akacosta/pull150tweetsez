# Pull150TweetsEZ

Want to pull 150 tweets easily because you're to lazy to go through the most recent tweets on an account. Look no furthur to use this Pull150TweetsEZ program! This program uses Twitter's API, and Panda to export into a excel sheet to make it easy for you to start your project faster! 

## Installing the program

```
sudo apt install pip python3
pip install pandas
pip install tweepy
git clone https://git.ucsc.edu/akacosta/pull150tweetsez.git
cd pull150tweetsez
python3 pull150tweetsez.py

```
## Note
You will need to input your "BEARER_TOKEN" with your own Twitter API credentials, which you can obtain from the Twitter Developer Dashboard (https://developer.twitter.com/en/apps).
