import tweepy
import pandas as pd
print()
print("Welcome to Pull150TweetsEZ!")
print()
bearer_data= input("Please input the bearer_token: ")
print()
user_data= input("Please enter the Twitter user name: ")


client = tweepy.Client(bearer_token= bearer_data)

user = client.get_user(username= user_data)

# Get the most recent 150 tweets from the target account
tweets = client.get_users_tweets(user.data.id,exclude = "replies,retweets", max_results= 100)

tweets_oldest = tweets.meta["oldest_id"]

tweetsContinue = client.get_users_tweets(user.data.id,until_id = tweets_oldest,exclude = "replies,retweets", max_results = 50)

# Create a list to store the tweets' text
tweetData = [tweet["text"] for tweet in tweets.data]
tweetDataContinue = [tweet["text"] for tweet in tweetsContinue.data]

for i in tweetDataContinue:
    tweetData.append(i)

# Create a dataframe from the list of tweet texts

df = pd.DataFrame({"Tweets": tweetData})

# Export the dataframe to an Excel file
df.to_excel("tweets.xlsx", index=False)
